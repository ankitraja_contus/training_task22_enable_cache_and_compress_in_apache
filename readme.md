# Requirement:Create php-apache image.In browser file should be serve php file and show the output hello world
    Step1: Create php file
    <?php
      echo "Hello World!";
      echo "PHP is so easy!";
    ?>
  # Step2:Create Dockerfile and create image.

    FROM php:7.2-apache
    COPY a.php /var/www/html/

    docker build -t apache-php .

# Step3:Create a service
    docker service create --publish 8001:80 --name=test apache-php
